export default class Mixdata {
    id: number;
    studentId: string;
    name: string;
    surname: string;
    activityname:string;
    activitydescription:string;
    Date:string;
    image: string;
    location:string;
    professor:string;
    periodstart:string;
    periodend:string;
    status:string;
  }
  