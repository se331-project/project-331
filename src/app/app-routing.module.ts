import {NgModule} from '@angular/core';
import { RouterModule,Routes } from '@angular/router';
import { StudentsViewComponent } from './students/view/students.view.component';
import { StudentsComponent } from './students/list/students.component';
import { StudentsAddComponent } from './students/add/students.add.component';
import { FileNotFoundComponent } from './shared/file-not-found/file-not-found.component';
import { MainPageComponent } from './main-page/main-page.component';
import { ProfessorNavComponent } from './professor/professor-nav/professor-nav.component';
import { AdminNavComponent } from './admin/admin-nav/admin-nav.component';
import { StudentNavComponent } from './students/student-nav/student-nav.component';
import { RegisterPageComponent } from './register-page/register-page.component';




const appRoutes: Routes = [


  
   {path:'', redirectTo:'/main', pathMatch:'full'},

   {path: 'main', component: MainPageComponent},
   {path: 'register', component: RegisterPageComponent},
   { path: 'professor', component: ProfessorNavComponent },
   { path: 'admin', component: AdminNavComponent },
   { path: 'student', component: StudentNavComponent },
   { path: '**', component: FileNotFoundComponent }

];

@NgModule({
    imports:[
        RouterModule.forRoot(appRoutes)
    ],
    exports:[
        RouterModule
    ]
})
export class AppRoutingModule{

}
   