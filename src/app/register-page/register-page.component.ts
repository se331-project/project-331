import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { FormControl, Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-register-page',
  templateUrl: './register-page.component.html',
  styleUrls: ['./register-page.component.css']
})
export class RegisterPageComponent implements OnInit {
  ngOnInit(): void {
    
  }
  form = this.fb.group({
    id: [''],
    studentId: ['',Validators.compose([Validators.required])],
    name: ['',Validators.compose([Validators.required])],
    surname: ['',Validators.compose([Validators.required])],
    dob: [''],
    image: [''],
    email: ['',Validators.compose([Validators.required, Validators.email])],
    password: ['',Validators.compose([Validators.required])],
 
  })

  validation_messages = {
    'studentId': [
      { type: 'required', message: 'Student Id is required'},
    ],
    'name': [
      {type: 'required', message: 'Please enter your name'}
    ],
    'surname': [
      { type: 'required', message: 'Please enter your surname'}
    ],
    'image': [],
    'birth': [],
    'email': [
      { type: 'required', message: 'Please enter your email'},
      { type: 'pattern', message: 'Email not in a correct format'}
    ],
    'password': [
      { type: 'required', message: 'Please enter your password'}
    ]
  };


  constructor(private fb:FormBuilder,public dialog: MatDialog,private router:Router){}
  
  submit(): void {
   
    Swal.fire({
      title: 'Are you sure?',
      //text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, submit it!'
    }).then((result) => {
      if (result.value) {
        Swal.fire(
          'success',
          '',
          'success'
        )
        this.router.navigate(["/main"]);
      }
    })
  }

  back(){
    this.router.navigate(["/main"]);
  }

}
