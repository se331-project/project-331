import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { ActivityService } from '../service/activity-service';
import Activity from '../entity/activity';

@Component({
  selector: 'app-my-nav',
  templateUrl: './my-nav.component.html',
  styleUrls: ['./my-nav.component.css']
})
export class MyNavComponent {

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  constructor(private breakpointObserver: BreakpointObserver,private activityService: ActivityService) {}

  activitys$: Observable<Activity[]> = this.activityService.getActivitys();

  main = true;
  student = false;
  pro = false;
  admin = false;

  mainnav() {
    this.main = true;
    this.student = false;
    this.pro = false;
    this.admin = false;
  }

  studentnav() {
    this.main = false;
    this.student = true;
    this.pro = false;
    this.admin = false;
  }

  pronav() {
    this.main = false;
    this.student = false;
    this.pro = true;
    this.admin = false;
  }

  adminnav() {
    this.main = false;
    this.student = false;
    this.pro = false;
    this.admin = true;
  }
  
}
