import { AfterViewInit, Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { ProfessorTableDataSource } from './professor-table-datasource';
import Student from 'src/app/entity/student';
import { BehaviorSubject } from 'rxjs';
import { ActivityService } from 'src/app/service/activity-service';
import Activity from 'src/app/entity/activity';
import { StudentService } from 'src/app/service/student-service';
import { MixdataService } from 'src/app/service/mixdata-service';
import Mixdata from 'src/app/entity/mixdata';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-professor-table',
  templateUrl: './professor-table.component.html',
  styleUrls: ['./professor-table.component.css']
})
export class ProfessorTableComponent implements AfterViewInit, OnInit {
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild(MatTable, {static: false}) table: MatTable<Mixdata>;
  dataSource: ProfessorTableDataSource;

  displayedColumns = ['studentId', 'name','surname','activityname','activitydescription','Date','location','professor','image','ApproveBtn','RejectBtn'];
  mixdata: Mixdata[];
  student:Student[];
 
  filter: string;
  filter$: BehaviorSubject<string>;
  constructor(private mixdataService : MixdataService) { }

  ngOnInit() {
    this.mixdataService.getMixdatas()
      .subscribe(mixdata => {
        this.dataSource = new ProfessorTableDataSource();
        this.dataSource.data = mixdata;
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        this.filter$ = new BehaviorSubject<string>('');
        this.dataSource.filter$ = this.filter$;
        this.table.dataSource = this.dataSource;
        this.mixdata = mixdata;
        
      }
      )

  }

  ngAfterViewInit() {
    
  }
  applyFilter(filterValue: string) {
    this.filter$.next(filterValue.trim().toLowerCase());
  }

  

  approve(){
    
      Swal.fire({
        title: 'Are you sure?',
        //text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, Approve it!'
      }).then((result) => {
        if (result.value) {
          Swal.fire(
            'Approved!',
            '',
            'success'
          )
        }
      })
    
  }

  reject(){
    
      Swal.fire({
        title: 'Are you sure?',
        //text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, Reject it!'
      }).then((result) => {
        if (result.value) {
          Swal.fire(
            'Rejected!',
            '',
            'success'
          )
        }
      })
    
  }

}