import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProfessorTableComponent } from './professor-table/professor-table.component';
import { Professorlist } from './list/professor-list.component';
import { ViewComponent } from './view/view.component';
import { ProfessorNavComponent } from './professor-nav/professor-nav.component';
import { MainPageComponent } from '../main-page/main-page.component';
import { ProAddComponent } from './add/add.component';

const ProfessorRoutes: Routes = [ 

    // { path: 'approve/professor', component: ProfessorTableComponent},
    // { path: 'list/professor', component: Professorlist},
    // { path: 'list/professor/update/:id' , component: ViewComponent}

    {
      path: 'professor', 
      component: ProfessorNavComponent ,
      children : [
          { path : '', redirectTo : 'list', pathMatch: 'full'},
          { path: 'approve', component: ProfessorTableComponent},
          { path: 'list', component: Professorlist},
          { path: 'list/detail/:id' , component: ViewComponent},
          { path: 'add' , component: ProAddComponent}
      ]
},
    { path: 'main', component: MainPageComponent}


    ];
   @NgModule({
     imports: [
       RouterModule.forRoot(ProfessorRoutes)
     ],
     exports: [
       RouterModule
       ]
   })
   export class ProfessorRoutingModule { }