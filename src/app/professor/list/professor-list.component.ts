import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import Student from 'src/app/entity/student';
import { BehaviorSubject } from 'rxjs';
import { ActivityService } from 'src/app/service/activity-service';
import Activity from 'src/app/entity/activity';
import { ProfessorlistDatasource } from './professor-list-datasource';
import { Router } from '@angular/router';


@Component({
  selector: 'app-list',
  templateUrl: './professor-list.component.html',
  styleUrls: ['./professor-list.component.css']
})
export class Professorlist implements AfterViewInit, OnInit {
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild(MatTable, {static: false}) table: MatTable<Activity>;
  dataSource: ProfessorlistDatasource;

  displayedColumns = ['id', 'activityname','activitydescription','period','Date','location','professor','image','OptionBtn'];
  activity: Activity[];
  filter: string;
  filter$: BehaviorSubject<string>;
  constructor(private activityService: ActivityService,private router: Router) { }

  ngOnInit() {
    this.activityService.getActivitys()
      .subscribe(activity => {
        this.dataSource = new ProfessorlistDatasource();
        this.dataSource.data = activity;
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        this.filter$ = new BehaviorSubject<string>('');
        this.dataSource.filter$ = this.filter$;
        this.table.dataSource = this.dataSource;
        this.activity = activity;
        
      }
      )
  }

  ngAfterViewInit() {
    
  }
  applyFilter(filterValue: string) {
    this.filter$.next(filterValue.trim().toLowerCase());
  }

  

}