import { Component, OnInit } from '@angular/core';
import Activity from 'src/app/entity/activity';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ActivityService } from 'src/app/service/activity-service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent implements OnInit {
  activitys: Activity[];
  activity: Activity;
  constructor(private route: ActivatedRoute,private activityService: ActivityService,private router: Router) { }

  ngOnInit():void {
    // this.activity = {
    //   'id' :1 ,
    //   'activityname' : '',
    //   'activitydescription':'',
    //   'period':'',
    //   'Date':'',
    //   'location':'',
    //   'professor':'',
    //   'image':''
    // };
    this.route.params
    .subscribe((params: Params) => {
      this.activityService.getActivity(params['id'])
      .subscribe((inputActivity: Activity) => this.activity = inputActivity);
    })
  }

  update(){
    Swal.fire({
      title: 'Are you sure?',
      //text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Update it!'
    }).then((result) => {
      if (result.value) {
        Swal.fire(
          'Updated!',
          '',
          'success'
        )
      }
    })
  }
}
