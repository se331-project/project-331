import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class ProAddComponent implements OnInit {

  constructor(private fb:FormBuilder,private router:Router) { }
  form = this.fb.group({
    
    name: ['',Validators.compose([Validators.required])],
    professor: ['',Validators.compose([Validators.required])],
    location: ['',Validators.compose([Validators.required])],
    description: [''],
    start: ['',Validators.compose([Validators.required])],
    end: ['',Validators.compose([Validators.required])],
    Date: ['',Validators.compose([Validators.required])],
    
  })

  validation_messages = {
    'name': [
      {type: 'required', message: 'Please enter the activity name'}
    ],
    'location': [
      { type: 'required', message: 'Please enter the activity location'}
    ],
    'professor': [
      { type: 'required', message: 'Please enter the professor'}
    ],
    'start': [
      { type: 'required', message: 'Please enter the periodStart'}
    ],
    'end': [
      { type: 'required', message: 'Please enter the periodEnd'}
    ],
    'Date': [
      { type: 'required', message: 'Please enter the Date'}
    ]
   
  };

  ngOnInit() {
  }

  add(): void {
    
    Swal.fire({
      title: 'Are you sure?',
      //text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, add it!'
    }).then((result) => {
      if (result.value) {
        Swal.fire(
          'Added!',
          '',
          'success'
        )
        this.router.navigate(["professor/list/detail/1"]);
      }
    })
  }
}
