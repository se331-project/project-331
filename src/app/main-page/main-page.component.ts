import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.css']
})
export class MainPageComponent implements OnInit {

  constructor(private router: Router) { }

  email = new FormControl('', [Validators.required, Validators.email]);

  getErrorMessage() {
    return this.email.hasError('required') ? '' :
        this.email.hasError('email') ? 'Not a valid email' :
            '';
  }

  username: string;
  password: string;

  ngOnInit() {
    
  }
  login() : void {
    
    

    if(this.username == 'admin@cmu.ac.th' && this.password == '1234'){
      
     this.router.navigate(["admin"]);
     const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 3000
    })
     Toast.fire({
      type: 'success',
      title: 'Signed in successfully'
    })
    } 
    else if (this.username == 'professor@cmu.ac.th' && this.password == '1234') {
      this.router.navigate(["professor"]);
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000
      })
      Toast.fire({
        type: 'success',
        title: 'Signed in successfully'
      })
    } 
    else if (this.username == 'student@cmu.ac.th' && this.password == '1234') {
      this.router.navigate(["student"]);
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000
      })
      Toast.fire({
        type: 'success',
        title: 'Signed in successfully'
      })
    } else {
      //alert("Invalid Username or Password");
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000
      })
      Toast.fire({
        type: 'error',
        title: 'Signed in failed'
      })
    }

  }

  register(): void{
    this.router.navigate(["/register"]);
  }
}
