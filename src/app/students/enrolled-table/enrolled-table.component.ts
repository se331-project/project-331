import { Component, OnInit, ViewChild, AfterViewInit, Output, Input } from '@angular/core';
import { MatPaginator, MatSort, MatTable, MatDialog } from '@angular/material';
import Student from '../../entity/student';
import { StudentTableDataSource } from '../student-table/student-table-datasource';
import { EnrolledTableDataSource } from './enrolled-table-datasource';
import Activity from '../../entity/activity';
import { BehaviorSubject } from 'rxjs';
import { ActivityService } from '../../service/activity-service';
import Mixdata from 'src/app/entity/mixdata';
import { MixdataService } from 'src/app/service/mixdata-service';
import { EventEmitter } from 'events';
import { Router } from '@angular/router';
import { ConfirmationDialogComponent } from 'src/app/shared/confirmation-dialog/confirmation-dialog.component';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-enrolled-table',
  templateUrl: './enrolled-table.component.html',
  styleUrls: ['./enrolled-table.component.css']
})
export class EnrolledTableComponent implements AfterViewInit,OnInit {

  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild(MatTable, {static: false}) table: MatTable<Mixdata>;
  dataSource: EnrolledTableDataSource;

  displayedColumns = ['activityname','description','period','Date','location','professor','image','Status','RemoveBtn'];
  mixdata: Mixdata[];
  filter: string;
  filter$: BehaviorSubject<string>;
  constructor(private mixdataService: MixdataService,public dialog: MatDialog,private router:Router) { }

  ngOnInit() {
    this.mixdataService.getMixdatas()
      .subscribe(mixdata => {
        this.dataSource = new EnrolledTableDataSource();
        this.dataSource.data = mixdata;
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        this.filter$ = new BehaviorSubject<string>('');
        this.dataSource.filter$ = this.filter$;
        this.table.dataSource = this.dataSource;
        this.mixdata = mixdata;
        
      }
      )
  }

  ngAfterViewInit() {
  }
  applyFilter(filterValue: string) {
    this.filter$.next(filterValue.trim().toLowerCase());
  }

  message = "Pending"

  receiveMessage($event) {
    this.message = $event
  }

  openDialog(): void {
    // const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
    //   width: '350px',
    //   data: "Do you confirm to remove this activity?"
    // });
    // dialogRef.afterClosed().subscribe(result => {
    //   if(result) {
    //     console.log('Yes clicked');
        
    //   }
    // });
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })
    
    swalWithBootstrapButtons.fire({
      title: 'Are you sure?',
      //text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, remove it!',
      cancelButtonText: 'No, cancel!',
      reverseButtons: true
    }).then((result) => {
      if (result.value) {
        swalWithBootstrapButtons.fire(
          'Deleted!',
          'Your event has been deleted.',
          'success'
        )
      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Cancelled',
          '',
          'error'
        )
      }
    })
  }
  

}
