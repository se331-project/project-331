import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { StudentService } from '../../service/student-service';
import Student from '../../entity/student';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-students-view',
  templateUrl: './students.view.component.html',
  styleUrls: ['./students.view.component.css']
})

export class StudentsViewComponent {
  students: Student[];
  student:Student;

  constructor(private route: ActivatedRoute, private studentService: StudentService){}

  ngOnInit(): void {
    this.route.params
     .subscribe((params: Params) => {
     this.studentService.getStudent(+params['id'])
     .subscribe((inputStudent: Student) => this.student = inputStudent);
     });
    
  }
  update(){
    Swal.fire({
      title: 'Are you sure?',
      //text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Update it!'
    }).then((result) => {
      if (result.value) {
        Swal.fire(
          'Updated!',
          '',
          'success'
        )
      }
    })
  }
}
