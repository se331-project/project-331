import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminTableComponent } from './admin-table/admin-table.component';
import { AddComponent } from './add/add.component';
import { AdminNavComponent } from './admin-nav/admin-nav.component';
import { MainPageComponent } from '../main-page/main-page.component';
const AdminRoutes: Routes = [ 
    // { path: 'list/admin', component: AdminTableComponent },
    // { path: 'add/admin', component: AddComponent }

    {
      path: 'admin', 
      component: AdminNavComponent,
      children : [
          { path : '', redirectTo : 'approve', pathMatch: 'full'},
          { path: 'approve', component: AdminTableComponent },
          { path: 'add', component: AddComponent }
      ]
},
    { path: 'main', component: MainPageComponent}

    ];
   @NgModule({
     imports: [
       RouterModule.forRoot(AdminRoutes)
     ],
     exports: [
       RouterModule
       ]
   })
   export class AdminRoutingModule { }