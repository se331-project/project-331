import { Injectable } from '@angular/core';
import { ActivityService } from './activity-service';
import { HttpClient } from '@angular/common/http';
import Activity from '../entity/activity';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ActivityFileImplService extends ActivityService{

  constructor(private http: HttpClient) {
    super();

  }

  getActivitys(): Observable<Activity[]> {
    return this.http.get<Activity[]>('assets/activity.json');
  }
  getActivity(id :number): Observable<Activity> {
    return this.http.get<Activity[]>('assets/activity.json')
    .pipe(map(activitys => {
      const output: Activity = 
      ( activitys as Activity[]).find
        (activity => activity.id === +id);
        return output;
    }));
  }
}