import { TestBed } from '@angular/core/testing';

import { ActivityFileImplService } from './activity-file-impl.service';

describe('ActivityFileImplService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ActivityFileImplService = TestBed.get(ActivityFileImplService);
    expect(service).toBeTruthy();
  });
});
